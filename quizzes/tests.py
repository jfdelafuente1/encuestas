from django.contrib.auth.models import User
from django.test import TestCase
from .models import Quiz, Question, Answer

class QuizModelTest(TestCase):

    @classmethod
    def setUp(self):
        self.me = User.objects.create_user(username='John', email='john@example.com', password='jfdelafuente')
        self.quiz = Quiz.objects.create(author=self.me, title='first quiz')
        self.question = Question.objects.create(quiz=self.quiz, prompt='¿ Cómo te llamas ?')
        self.question2 = Question.objects.create(quiz=self.quiz, prompt='¿ Dónde vives ?')
        self.answer_uno = Answer.objects.create(question=self.question, text="Luis", correct=False)
        self.answer_dos = Answer.objects.create(question=self.question, text="John", correct=True)
        self.answer_tres = Answer.objects.create(question=self.question, text="Pedro", correct=False)



    def test_title_content(self):
        quiz = Quiz.objects.get(id=1)
        expected_object_name = f'{quiz.title}'
        self.assertEquals(expected_object_name, 'first quiz')

    def test_question_count(self):
        self.assertEqual(self.quiz.question_count, 2)

