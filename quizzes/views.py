from quizzes.tests import QuizModelTest
from rest_framework import generics

from .models import Quiz
from .serializers import QuizSerializer


class ListQuiz(generics.ListCreateAPIView):
    queryset = Quiz.objects.all()
    serializer_class = QuizSerializer


class DetailQuiz(generics.RetrieveUpdateDestroyAPIView):
    queryset = Quiz.objects.all()
    serializer_class = QuizSerializer